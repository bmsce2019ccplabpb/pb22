#include<stdio.h>
void swap(int*a,int*b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("enter two numbers");
scanf("%d %d",&n1,&n2);
printf("\nthe value of numbers before calling the function is %d %d",n1,n2);
swap(&n1,&n2);
printf("\nthe value of numbers after calling the function is %d %d",n1,n2);
return 0;
}